import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.BorderLayout;
import javax.swing.SwingConstants;
import javax.swing.JPanel;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextPane;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.JScrollBar;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import javax.swing.JSlider;
import javax.swing.ButtonGroup;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Color;
import javax.swing.AbstractListModel;

public class vista {

	private JFrame frame;
	private JTextField textField;
	int num1 = 0;
	int num2 = 0;
	int signe = 0;
	String operacion = "";
	ArrayList<String> operaciones = new ArrayList<String>();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					vista window = new vista();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public vista() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	
	public static int numeroXifras(int numero) {
		int cifras= 0;    
        while(numero!=0){             
                numero = numero/10;         
               cifras++;         
        }
		return cifras;
	}
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("CALCULADORA");
		frame.getContentPane().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			}
		});
		frame.setBounds(100, 100, 640, 535);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		textField = new JTextField();
		textField.setBackground(Color.LIGHT_GRAY);
		textField.setFont(new Font("Yu Gothic UI Light", Font.PLAIN, 30));
		textField.setColumns(10);
		JLabel lblNewLabel_1 = new JLabel("");
		JButton btnNewButton = new JButton("7");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				if(signe==0) {
					if(num1==0) {
						num1 = num1 + 7;
					
					}else {
						
						num1 = num1 + (7*(int)(Math.pow(10, numeroXifras(num1))));
						
					}
					textField.setText(num1 +"");				
				} else {
					if(numeroXifras(num2)==0)
						num2 = num2 + 7;
					else {
						num2 = num2 + (7*(int)(Math.pow(10, numeroXifras(num2))));
					}
					textField.setText(num2 +"");
				}
			}
		});
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});
		
		JButton btnNewButton_1 = new JButton("8");
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(signe==0) {
					if(num1==0) {
						num1 = num1 + 8;
					
					}else {
						System.out.println(numeroXifras(num1));
						num1 = num1 + (8*(int)(Math.pow(10, numeroXifras(num1))));
						
					}
					textField.setText(num1 +"");				
				} else {
					if(numeroXifras(num2)==0)
						num2 = num2 + 8;
					else {
						num2 = num2 + (8*(int)(Math.pow(10, numeroXifras(num2))));
					}
					textField.setText(num2 +"");
				}
			}
		});
		
		JButton btnNewButton_2 = new JButton("/");
		btnNewButton_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				signe = 1;
			
			}
		});
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		
		JButton btnNewButton_2_1 = new JButton("9");
		btnNewButton_2_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(signe==0) {
					if(num1==0) {
						num1 = num1 + 9;
					
					}else {
						System.out.println(numeroXifras(num1));
						num1 = num1 + (9*(int)(Math.pow(10, numeroXifras(num1))));
						
					}
					textField.setText(num1 +"");					
				} else {
					if(numeroXifras(num2)==0)
						num2 = num2 + 9;
					else {
						num2 = num2 + (9*(int)(Math.pow(10, numeroXifras(num2))));
					}
					textField.setText(num2 +"");
				}
			}
		});
		
		JButton btnNewButton_3 = new JButton("5");
		btnNewButton_3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(signe==0) {
					if(num1==0) {
						num1 = num1 + 5;
					
					}else {
						System.out.println(numeroXifras(num1));
						num1 = num1 + (5*(int)(Math.pow(10, numeroXifras(num1))));
						
					}
					textField.setText(num1 +"");
									
				} else {
					if(numeroXifras(num2)==0)
						num2 = num2 + 5;
					else {
						num2 = num2 + (5*(int)(Math.pow(10, numeroXifras(num2))));
					}
					textField.setText(num2 +"");
				}
			}
		});
		
		JButton btnNewButton_4 = new JButton("6");
		btnNewButton_4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(signe==0) {
					if(num1==0) {
						num1 = num1 + 6;
					
					}else {
						System.out.println(numeroXifras(num1));
						num1 = num1 + (6*(int)(Math.pow(10, numeroXifras(num1))));
						
					}
					textField.setText(num1 +"");
									
				} else {
					if(numeroXifras(num2)==0)
						num2 = num2 + 6;
					else {
						num2 = num2 + (6*(int)(Math.pow(10, numeroXifras(num2))));
					}
					textField.setText(num2 +"");
				}
			}
		});
		
		JButton btnX = new JButton("X");
		btnX.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				signe = 3;
				
			}
		});
		btnX.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		
		JButton btnNewButton_6 = new JButton("4");
		btnNewButton_6.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(signe==0) {
					if(num1==0) {
						num1 = num1 + 4;
					
					}else {
						System.out.println(numeroXifras(num1));
						num1 = num1 + (4*(int)(Math.pow(10, numeroXifras(num1))));
						
					}
					textField.setText(num1 +"");
				} else {
					if(numeroXifras(num2)==0)
						num2 = num2 + 4;
					else {
						num2 = num2 + (4*(int)(Math.pow(10, numeroXifras(num2))));
					}
					textField.setText(num2 +"");
				}
			}
		});
		
		JButton btnNewButton_7 = new JButton("2");
		btnNewButton_7.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(signe==0) {
					if(num1==0) {
						num1 = num1 + 2;
					
					}else {
						System.out.println(numeroXifras(num1));
						num1 = num1 + (2*(int)(Math.pow(10, numeroXifras(num1))));
						
					}
					textField.setText(num1 +"");				
				} else {
					if(numeroXifras(num2)==0)
						num2 = num2 + 2;
					else {
						num2 = num2 + (2*(int)(Math.pow(10, numeroXifras(num2))));
					}
					textField.setText(num2 +"");
				}
			}
		});
		btnNewButton_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		
		JButton btnNewButton_8 = new JButton("3");
		btnNewButton_8.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(signe==0) {
					if(num1==0) {
						num1 = num1 + 3;
					
					}else {
						System.out.println(numeroXifras(num1));
						num1 = num1 + (3*(int)(Math.pow(10, numeroXifras(num1))));
						
					}
					textField.setText(num1 +"");				
				} else {
					if(numeroXifras(num2)==0)
						num2 = num2 + 3;
					else {
						num2 = num2 + (3*(int)(Math.pow(10, numeroXifras(num2))));
					}
					textField.setText(num2 +"");
				}
			}
		});
		
		JButton btnNewButton_9 = new JButton("=");
		btnNewButton_9.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				double resultat = 0;
				switch (signe) {
				case 1:
					resultat = num1/num2;
					operacion = num1 + " / " + num2 + " = " + resultat;
					operaciones.add(operacion);
					break;
				case 2:
					resultat =num1+num2;
					operacion = num1 + " + " + num2 + " = " + resultat;
					operaciones.add(operacion);
					break;
				case 3:
					resultat = num1*num2;
					operacion = num1 + " x " + num2 + " = " + resultat;
					operaciones.add(operacion);
					break;
				case 4:
					resultat = num1-num2;
					operacion = num1 + " - " + num2 + " = " + resultat;
					operaciones.add(operacion);
					break;

				default:
					break;
				}
				textField.setText(""+resultat);
				num1 = 0; num2 = 0; signe = 0;
				Object[] resultats = operaciones.toArray();
				int contador = 0; String resultatsString ="";
				while(contador<resultats.length) {
					resultatsString = resultatsString + "\n\n > " + resultats[contador];
					lblNewLabel_1.setText(""+resultats[contador]);
					contador++;
				}
				//lblNewLabel_1.setText(resultatsString);
				
				
			}
		});
		
		JButton btnNewButton_10 = new JButton("1");
		btnNewButton_10.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(signe==0) {
					if(num1==0) {
						num1 = num1 + 1;
					
					}else {
						System.out.println(numeroXifras(num1));
						num1 = num1 + (1*(int)(Math.pow(10, numeroXifras(num1))));
						
					}
					textField.setText(num1 +"");				
				} else {
					if(numeroXifras(num2)==0)
						num2 = num2 + 1;
					else {
						num2 = num2 + (1*(int)(Math.pow(10, numeroXifras(num2))));
					}
					textField.setText(num2 +"");
				}
			}
		});
		
		JButton btnX_1 = new JButton("-");
		btnX_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnX_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				signe = 4;
				
			}
		});
		
		JButton btnX_2 = new JButton("+");
		btnX_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnX_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				signe = 2;
				
			}
		});
		
		JLabel lblNewLabel = new JLabel("HISTORIAL");
		lblNewLabel.setFont(new Font("Yu Gothic Light", Font.PLAIN, 26));
		
		JButton btnNewButton_5 = new JButton("C");
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		
		JButton btnNewButton_11 = new JButton("CE");
		
		JButton btnNewButton_12 = new JButton("%");
		btnNewButton_12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		
		JButton btnNewButton_13 = new JButton("BORRAR");
		
		JButton btnNewButton_14 = new JButton("1/x");
		
		JButton btnNewButton_15 = new JButton("x^2");
		
		JButton btnNewButton_16 = new JButton("x^1/2");
		
		JButton btnNewButton_17 = new JButton("+/-");
		
		JButton btnNewButton_18 = new JButton("0");
		
		JButton btnNewButton_19 = new JButton(",");
		
		JList list = new JList();
		list.setModel(new AbstractListModel() {
			String[] values = new String[] {"dsd", "sdds", "dsdsd"};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		
		
		
		
		
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(btnNewButton_12, GroupLayout.DEFAULT_SIZE, 84, Short.MAX_VALUE)
										.addComponent(btnNewButton_14, GroupLayout.DEFAULT_SIZE, 84, Short.MAX_VALUE))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
											.addComponent(btnNewButton_15, GroupLayout.DEFAULT_SIZE, 83, Short.MAX_VALUE)
											.addComponent(btnNewButton_1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
										.addComponent(btnNewButton_11, GroupLayout.DEFAULT_SIZE, 83, Short.MAX_VALUE))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
										.addComponent(btnNewButton_2_1, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 88, Short.MAX_VALUE)
										.addComponent(btnNewButton_16, GroupLayout.PREFERRED_SIZE, 88, Short.MAX_VALUE)
										.addComponent(btnNewButton_5, GroupLayout.DEFAULT_SIZE, 88, Short.MAX_VALUE))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(btnX_1, GroupLayout.DEFAULT_SIZE, 91, Short.MAX_VALUE)
										.addComponent(btnNewButton_13, GroupLayout.DEFAULT_SIZE, 91, Short.MAX_VALUE)
										.addComponent(btnNewButton_2, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 91, Short.MAX_VALUE)))
								.addComponent(textField, GroupLayout.DEFAULT_SIZE, 364, Short.MAX_VALUE))
							.addGap(25)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 142, GroupLayout.PREFERRED_SIZE)
									.addGap(79))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(lblNewLabel_1)
									.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(list)
									.addGap(191))))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(btnNewButton_17, GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE)
								.addComponent(btnNewButton_10, GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE)
								.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnNewButton_6, GroupLayout.DEFAULT_SIZE, 84, Short.MAX_VALUE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addComponent(btnNewButton_18, 0, 0, Short.MAX_VALUE)
								.addComponent(btnNewButton_7, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(btnNewButton_3, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.UNRELATED)))
							.addGap(7)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(btnNewButton_19, 0, 0, Short.MAX_VALUE)
								.addComponent(btnNewButton_8, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnNewButton_4, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addComponent(btnX, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnNewButton_9, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnX_2, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE))
							.addGap(246))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(25)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(18)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
									.addComponent(btnNewButton_13, GroupLayout.DEFAULT_SIZE, 52, Short.MAX_VALUE)
									.addComponent(btnNewButton_12, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
									.addComponent(btnNewButton_11, GroupLayout.DEFAULT_SIZE, 51, Short.MAX_VALUE)
									.addComponent(btnNewButton_5, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
								.addComponent(lblNewLabel_1))
							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(btnNewButton_14, GroupLayout.DEFAULT_SIZE, 51, Short.MAX_VALUE)
								.addComponent(btnNewButton_15, GroupLayout.DEFAULT_SIZE, 51, Short.MAX_VALUE)
								.addComponent(btnNewButton_16, GroupLayout.DEFAULT_SIZE, 51, Short.MAX_VALUE)
								.addComponent(btnNewButton_2, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnNewButton_1, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnNewButton_2_1, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnX_1, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(btnNewButton_3, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnNewButton_4, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnX_2, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnNewButton_6, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(btnNewButton_10, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnNewButton_7, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnNewButton_8, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnX, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addComponent(btnNewButton_19, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnNewButton_18, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnNewButton_17, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnNewButton_9, GroupLayout.DEFAULT_SIZE, 54, Short.MAX_VALUE))
							.addGap(52))
						.addGroup(groupLayout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(list)
							.addContainerGap())))
		);
		frame.getContentPane().setLayout(groupLayout);
	}
}
