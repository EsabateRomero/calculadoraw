package odt;

import java.io.ObjectInputStream.GetField;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class operacions {
	double numero1 = 0;
	double numero2 = 0;
	String signe =  "";
	String operacio = "";
	String resultat = "";
	boolean op1num = false;

	
	public operacions() {
		
	}
	

	public double suma() {
		return numero1+numero2;
	}
	
	public double resta() {
		return numero1+numero2;
	}
	
	public double multiplicacion() {
		return numero1*numero2;
	}
	
	public double division() {
		return numero1/numero2;
	}

	public double percentatge() {
		return numero1*numero2/100;
	}
	
	public void C() {
		this.numero1 = 0;
		this.numero2 = 0;
		this.signe = "";
	}

	public void CE() {
		if(signe=="")
			this.numero1 = 0;
		else
			this.numero2 = 0;
	}
	
	public void borrarUltimaCifra(){
		if(signe=="")
			this.numero1 = numero1/10;
		else
			this.numero2 = numero2/10;
	}

	public void botoIgual() {
		double r;
		DecimalFormat df = new DecimalFormat("#.00");
		if(numero1!=0&&numero2!=0&&!signe.equals("")) {
			switch (signe) {
			case "x":
				r = numero1*numero2;
				this.resultat = df.format(r);
				this.operacio = this.numero1 + " x " + this.numero2 + " = " + this.resultat;		
				break;
			case "/":
				r = numero1/numero2;
				this.resultat = df.format(r);
				this.operacio = this.numero1 + " / " + this.numero2 + " = " + this.resultat;		
				break;
				
			case "+":
				r = numero1 + numero2;
				this.resultat = df.format(r);
				this.operacio = this.numero1 + " + " + this.numero2 + " = " + this.resultat;		
				break;
				
			case "-":
				r = numero1 - numero2;
				this.resultat = df.format(r);
				this.operacio = this.numero1 + " - " + this.numero2 + " = " + this.resultat;		
				break;

			}
				
		}
	}
	
	public String getOperacio() {
		return operacio;
	}

	public void setOperacio(String operacio) {
		this.operacio = operacio;
	}
	

	public String getSigne() {
		return signe;
	}
	
	public double getNumero1() {
		return numero1;
	}
	
	public double getNumero2() {
		return numero2;
	}

	public String getResultat() {
		return resultat;
	}

	public void cambiarSigne() {
		if(signe=="")
			this.numero1 = numero1*(-1);
		else
			this.numero2 = numero2*(-1);
	}

	public void setNumero(int numero) {
		if(signe=="")
			if(numero1==0) {
			this.numero1 = numero;
			}else
			this.numero1 = numero1*10 + numero;
		else
			if(numero2==0)
			this.numero2 = numero;
			else
			this.numero2 =numero2*10 + numero;
	}



	public void setSigne(String signe) {
		this.signe = signe; double r;
		DecimalFormat df = new DecimalFormat("#.00");
			switch (this.signe) {
			case "1/x":
				op1num = true;
				r = 1/this.numero1;
				this.resultat = df.format(r);
				this.operacio = "1 / " + this.numero1 + " = " + this.resultat;				
				break;
				
			case "x^1/2":
				op1num = true;
				r = Math.sqrt(this.numero1);
				this.resultat = df.format(r);
				this.operacio = this.numero1 + " ^1/2 = " + this.resultat;	
				break;

			case "x^2":
				op1num = true;
				r= this.numero1*this.numero1;
				this.resultat = df.format(r);
				this.operacio = this.numero1 + " ^2 = " + this.resultat;
				break;
			
			case "x":
				this.signe = "x";
				break;

			case "/":
				this.signe = "/";
				break;
				
			case "+":
				this.signe = "+";
				break;
				
			case "-":
				this.signe = "-";
				break;
			
		}
		this.signe = signe;
		
	}

	@Override
	public String toString() {
		String op= "";
		if(op1num) 
			op = numero1 + signe + " = " + resultat;
		else
			op = numero1 + signe + numero2 + " = " + resultat;
		return op;
		
	}

	
	
	public double getNumero() {
		if(signe=="")
			return numero1;
		else
			return numero2;	
	}


	
}
