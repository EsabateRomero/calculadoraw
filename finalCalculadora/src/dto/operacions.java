package dto;

import java.util.ArrayList;

public class operacions {
	double numero1 = 0;
	double numero2 = 0;
	String signe =  "";
	String operacio = "";
	double resultat = 0;
	static String[] signeFinal = {"1/x","x^2","x^1/2"};
	boolean operacioDunNumero = false;
	
	public operacions() {
		
	}
	
	
	public double suma() {
		return numero1+numero2;
	}
	
	public double resta() {
		return numero1+numero2;
	}
	
	public double multiplicacion() {
		return numero1*numero2;
	}
	
	public double division() {
		return numero1/numero2;
	}

	public double percentatge() {
		return numero1*numero2/100;
	}
	
	public void CE() {
		this.numero1 = 0;
		this.numero2 = 0;
	}
	
	public void C() {
		if(signe=="")
			this.numero1 = 0;
		else
			this.numero2 = 0;
	}
	
	public void borrarUltimaCifra(){
		if(signe=="")
			this.numero1 = numero1/10;
		else
			this.numero2 = numero2/10;
	}

	public void setNumero(double numero) {
		if(signe=="")
			if(numero1==0) {
			this.numero1 = numero;
		System.out.println("operacions.setNumero()");
			}else
			this.numero1 = numero1*10 + numero;
		else
			if(numero2==0)
			this.numero2 = numero;
			else
			this.numero2 =numero2*10 + numero;
	}

	
	public String getOperacio() {
		return operacio;
	}

	public void setOperacio(String operacio) {
		this.operacio = operacio;
	}
	

	public String getSigne() {
		return signe;
	}

	public void setSigne(String signe) {
		if(operacioDunNumero(signe))
			operacioDunNumero =  true;
		this.signe = signe;
	}

	@Override
	public String toString() {
		return "operacions [numero1=" + numero1 + ", numero2=" + numero2 + ", operacio=" + operacio + "]";
	}

	
	
	public double getNumero1() {
		return numero1;
	}


	public double getNumero2() {
		return numero2;
	}


	public boolean operacioDunNumero(String string) {
		int contador = 0; boolean estar = false;
		if(contador<signeFinal.length) {
			if(string.contentEquals(signeFinal[contador]))
				estar = true;
			contador++;
		}
		return estar;
	}
	
	
}
