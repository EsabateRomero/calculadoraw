package finalCalculadora;

import java.awt.EventQueue;

import vistas.vistas;

public class calculadoraApp {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					vistas window = new vistas();
					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}
}

